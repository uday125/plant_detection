import skimage
import numpy as np
import tensorflow as tf
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn.config import Config


class PlantConfig(Config):
    NAME = "Plant"

    IMAGES_PER_GPU = 2

    NUM_CLASSES = 1 + 2  

    DETECTION_MIN_CONFIDENCE = 0.85

config_plant = PlantConfig()
class InferenceConfig(config_plant.__class__):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config_plant = InferenceConfig()

DEVICE = "/gpu:0" 
TEST_MODE = "inference"
with tf.device(DEVICE):
    model_plant = modellib.MaskRCNN(mode="inference", model_dir="MODEL_DIR",
                              config=config_plant)

weights_path_plant = "plant_v3.h5"
model_plant.load_weights(weights_path_plant, by_name=True)

image = "test1.jpg"
image = skimage.io.imread(image)
results_plant = model_plant.detect([image], verbose=1)
print("done")


    